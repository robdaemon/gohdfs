package gohdfs

import "testing"

func TestConnect(t *testing.T) {
	handle := Connect("default", 0)

	if handle == nil {
		t.Error("Unable to connect to HDFS")
		t.FailNow()
	}

	t.Logf("Connected to HDFS %s\n", handle)

	Disconnect(handle)

	t.Logf("Disconnected from HDFS")	
}