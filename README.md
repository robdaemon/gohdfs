gohdfs
======

A Go wrapper for libhdfs

Building
--------

    CGO_CFLAGS='-I/Users/rroland/Java/hadoop-1.1.2/src/c++/libhdfs -I/System/Library/Frameworks/JavaVM.framework/Headers' CGO_LDFLAGS='-lhdfs -L/opt/local/lib' go install

Replace pathnames accordingly.

Tests
-----

Running the tests requires access to the Hadoop classpath, and a running Hadoop instance on localhost.

    CLASSPATH=`/opt/local/share/java/hadoop-1.1.1/bin/hadoop classpath` go test

Replace pathnames accordingly.

License
-------

Distributed under the terms of the Apache License 2.0. See LICENSE.